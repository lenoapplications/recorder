package recorder.components.selenium.base.driverContext;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;



public class DriverContext {
    private final WebDriver driver;
    private final EventFiringWebDriver eventFiringWebDriver;

    public DriverContext(WebDriver driver) {
        eventFiringWebDriver = new EventFiringWebDriver(driver);
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public EventFiringWebDriver getEventFiringWebDriver(){
        return eventFiringWebDriver;
    }




}
