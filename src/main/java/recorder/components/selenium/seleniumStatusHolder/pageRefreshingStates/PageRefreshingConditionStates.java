package recorder.components.selenium.seleniumStatusHolder.pageRefreshingStates;

public class PageRefreshingConditionStates {
    private boolean codeReadyForRefresh = false;


    public boolean isCodeReadyForRefresh(){
        return codeReadyForRefresh;
    }
    public void codeReadyForRefresh(){
        codeReadyForRefresh = true;
    }
    public void reset(){
        codeReadyForRefresh = false;
    }
}
