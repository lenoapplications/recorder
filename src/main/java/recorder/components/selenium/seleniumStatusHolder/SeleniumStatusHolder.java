package recorder.components.selenium.seleniumStatusHolder;

import recorder.components.selenium.seleniumStatusHolder.pageRefreshingStates.PageRefreshingConditionStates;

public class SeleniumStatusHolder {
    private final PageRefreshingConditionStates pageRefreshingConditionStates = new PageRefreshingConditionStates();
    private String currentURL;


    public String getCurrentURL() {
        return currentURL;
    }

    public void setCurrentURL(String currentURL) {
        this.currentURL = currentURL;
    }

    public PageRefreshingConditionStates getPageRefreshingConditionStates(){
        return pageRefreshingConditionStates;
    }
}
