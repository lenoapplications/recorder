package recorder.components.events.eventsPerformModel.eventsPerformModelImpl.documentsEvents;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;
import recorder.components.events.eventHolder.eventsImpl.dto.DocumentEventListenersDto;
import recorder.components.exception.performException.PerformExceptionModel;
import recorder.components.exception.performException.exceptions.WaitForElementToBeLocatedException;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.events.eventsPerformModel.EventsPerformAbstract;
import recorder.components.exception.performException.PerformException;

import java.util.HashMap;

public class Click extends EventsPerformAbstract {

    public Click(DriverManager driverManager, JsManager jsManager, ExecutorManager executorManager) {
        super(driverManager,jsManager, executorManager);
    }

    @Override
    public void perform(SerializableEventsDto serializableEventsDto) throws PerformException {
        String xpath = ((DocumentEventListenersDto)serializableEventsDto).getXpath();
        waitForPresenceOfElement(xpath);
        WebElement webElement = getWebElement(xpath);
        webElement.click();
    }

    @Override
    public boolean tryPerformExceptionSolution(HashMap<String,String> report, SerializableEventsDto serializableEventsDto, PerformExceptionModel performExceptionModel) {
        if (performExceptionModel.solve(this,report)){
            if (performExceptionModel instanceof WaitForElementToBeLocatedException){
                return performAgain(serializableEventsDto);
            }
        }
        return false;
    }
    private boolean performAgain(SerializableEventsDto serializableEventsDto){
        try {
            perform(serializableEventsDto);
            return true;
        }catch (PerformException performException){
            return false;
        }
    }



}
