package recorder.components.events.eventsPerformModel.eventsPerformModelImpl.documentsEvents;

import org.openqa.selenium.WebElement;
import recorder.components.exception.performException.PerformExceptionModel;
import recorder.components.exception.performException.exceptions.WaitForElementToBeLocatedException;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.events.eventHolder.eventsImpl.dto.DocumentEventListenersDto;
import recorder.components.events.eventsPerformModel.EventsPerformAbstract;
import recorder.components.exception.performException.PerformException;

import javax.xml.xpath.XPath;
import java.util.HashMap;

public class Input extends EventsPerformAbstract {
    public Input(DriverManager driverManager, JsManager jsManager, ExecutorManager executorManager) {
        super(driverManager,jsManager, executorManager);
    }

    @Override
    public void perform(SerializableEventsDto serializableEventsDto) throws PerformException {
        DocumentEventListenersDto documentEventListeners = (DocumentEventListenersDto) serializableEventsDto;
        if(documentEventListeners.getValue() != null){
            String xpath = ((DocumentEventListenersDto)serializableEventsDto).getXpath();
            waitForPresenceOfElement(xpath);
            WebElement webElement = getWebElement(xpath);
            webElement.sendKeys(documentEventListeners.getValue());
        }
    }

    @Override
    public boolean tryPerformExceptionSolution(HashMap<String,String> report, SerializableEventsDto serializableEventsDto, PerformExceptionModel performExceptionModel) {
        if (performExceptionModel.solve(this,report)){
            if (performExceptionModel instanceof WaitForElementToBeLocatedException){
                return performAgain(serializableEventsDto);
            }
        }
        return false;
    }
    private boolean performAgain(SerializableEventsDto serializableEventsDto){
        try {
            perform(serializableEventsDto);
            return true;
        }catch (PerformException performException){
            return false;
        }
    }
}
