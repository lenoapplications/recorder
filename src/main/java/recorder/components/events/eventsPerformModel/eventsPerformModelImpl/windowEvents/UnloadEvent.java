package recorder.components.events.eventsPerformModel.eventsPerformModelImpl.windowEvents;

import recorder.components.exception.performException.PerformExceptionModel;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.events.eventsPerformModel.EventsPerformAbstract;

import java.util.HashMap;

public class UnloadEvent extends EventsPerformAbstract {

    public UnloadEvent(DriverManager driverManager, JsManager jsManager, ExecutorManager executorManager) {
        super(driverManager,jsManager, executorManager);
    }

    @Override
    public void perform(SerializableEventsDto serializableEvents) {
        refreshJsCode();
    }

    @Override
    public boolean tryPerformExceptionSolution(HashMap<String, String> report, SerializableEventsDto serializableEventsDto, PerformExceptionModel performExceptionModel) {
        return false;
    }


}
