package recorder.components.events.eventsPerformModel;

import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.performException.PerformException;
import recorder.components.exception.performException.PerformExceptionModel;

import java.util.HashMap;

public interface EventsPerformModel{
    void perform(SerializableEventsDto serializableEvents) throws PerformException;
    boolean tryPerformExceptionSolution(HashMap<String,String>report, SerializableEventsDto serializableEventsDto, PerformExceptionModel performExceptionModel);
}
