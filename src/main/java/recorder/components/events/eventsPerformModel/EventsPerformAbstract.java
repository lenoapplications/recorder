package recorder.components.events.eventsPerformModel;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import recorder.components.exception.performException.exceptions.WaitForElementToBeLocatedException;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.exception.performException.PerformException;

public abstract class EventsPerformAbstract implements EventsPerformModel {
    private final JsManager jsManager;
    private final ExecutorManager executorManager;
    private final DriverManager driverManager;

    protected EventsPerformAbstract(DriverManager driverManager,JsManager jsManager, ExecutorManager executorManager) {
        this.jsManager = jsManager;
        this.executorManager = executorManager;
        this.driverManager = driverManager;
    }
    public WebElement getWebElement(String xpath) throws PerformException {
        try {
            WebElement webElement = jsManager.uploadCode(executorManager,"return window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.extractWebElement(arguments[0]);",xpath);
            if (webElement == null ){
                throw new NullPointerException(WaitForElementToBeLocatedException.CAUSE_INVALID_SELECTOR);
            }else{
                return webElement;
            }
        }catch (WebDriverException | NullPointerException e){
            WaitForElementToBeLocatedException waitForElementToBeLocatedException = new WaitForElementToBeLocatedException(e.getMessage());
            throw new PerformException(waitForElementToBeLocatedException);
        }

    }
    public void refreshJsCode(){
        jsManager.uploadCompleteCode(executorManager);
    }

    public boolean waitForPresenceOfElement(String xpath)throws PerformException{
        try {
            while(!jsManager.uploadWaitForPresenceOfElement(executorManager,xpath)){
                System.out.println("waiting for presence");
            }
            return true;
        }catch (TimeoutException t){
            WaitForElementToBeLocatedException waitForElementToBeLocatedException = new WaitForElementToBeLocatedException(t.getCause().getMessage());
            throw new PerformException(waitForElementToBeLocatedException);
        }
        catch (WebDriverException t){
            WaitForElementToBeLocatedException waitForElementToBeLocatedException = new WaitForElementToBeLocatedException(t.getMessage());
            throw new PerformException(waitForElementToBeLocatedException);
        }
    }

    public boolean isDocumentComplete(){
        boolean isDocumentComplete = jsManager.uploadRefreshCheckerCode(executorManager);
        return isDocumentComplete;
    }
}
