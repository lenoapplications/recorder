package recorder.components.events.assertionCodeRunner;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import org.codehaus.groovy.control.CompilationFailedException;
import org.openqa.selenium.WebElement;
import recorder.components.events.assertionCodeRunner.assertionAPI.AssertHelper;
import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.assertException.exceptions.AssertReturnValueException;

import java.util.List;

public class AssertionCodeRunner {

    public static boolean runAssertionCode(String code, List<WebElement> webElements )throws AssertExceptionAbstract {
        try {
            Binding binding = new Binding();
            GroovyShell groovyShell = new GroovyShell(binding);
            String finalCode = setup(code,binding,webElements);
            Boolean status = (Boolean) groovyShell.evaluate(finalCode);
            if (status == null){
                throw new AssertReturnValueException();
            }else{
                return status;
            }
        }catch (CompilationFailedException e){
            System.out.println(e.getUnit().getErrorCollector().getErrors().get(0));
            throw new AssertReturnValueException();
        }
    }

    private static String setup(String codeToRun,Binding binding, List<WebElement> webElements){
        String[] params = setupWebElementsParam(binding,webElements);
        setupAssertHelperParam(binding);
        return wrapCodeToRunWithMainScript(params,codeToRun);
    }
    private static void setupAssertHelperParam(Binding binding){
        binding.setVariable("assrtHlpr",new AssertHelper());
    }
    private static String[] setupWebElementsParam(Binding binding,List<WebElement> webElements){
        String firstParam = "";
        String secondParam = "";
        String thirdParam = "";
        int size = webElements.size();
        for (int i = 0; i < size; i++){
            String param = String.format("webElement%s", Integer.toString(i));
            String tagName = webElements.get(i).getTagName().concat(Integer.toString(i));

            firstParam = firstParamStringSetup(firstParam,tagName,i,size-1);
            secondParam = secondParamStringSetup(secondParam,tagName,i,size - 1);
            thirdParam = thirdParamStringSetup(thirdParam,param,i,size-1);

            binding.setVariable(param,webElements.get(i));
        }
        return new String[]{firstParam,secondParam,thirdParam};
    }

    private static String firstParamStringSetup(String params,String webParam,int index,int lastIndex){
        params = params.concat(",").concat(webParam);
        return params;
    }
    private static String secondParamStringSetup(String params,String webParam,int index,int lastIndex){
        if ((index == lastIndex) || (index == 0 && index == lastIndex)){
            params = params.concat(webParam);
        }else{
            params = params.concat(webParam).concat(",");
        }
        return params;
    }
    private static String thirdParamStringSetup(String params,String webParam,int index,int size){
        params = params.concat(",").concat(webParam);
        return params;
    }

    private static String wrapCodeToRunWithMainScript(String[] params,String codeToRun){
        String wrapper = "{assertHelper%s-> Closure codeToRunClosure=%s; codeToRunClosure.call(%s)}.call(assrtHlpr%s)";
        return String.format(wrapper,params[0],codeToRun,params[1],params[2]);
    }

}
