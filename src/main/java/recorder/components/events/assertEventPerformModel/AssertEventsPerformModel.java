package recorder.components.events.assertEventPerformModel;


import recorder.components.exception.assertException.AssertExceptionAbstract;

import java.util.HashMap;

public interface AssertEventsPerformModel {
    boolean assertEvent() throws AssertExceptionAbstract;
    boolean tryAssertExceptionSolution(HashMap<String,String> report,AssertExceptionAbstract assertExceptionAbstract);
}
