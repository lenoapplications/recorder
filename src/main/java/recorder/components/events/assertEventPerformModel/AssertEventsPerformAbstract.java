package recorder.components.events.assertEventPerformModel;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.assertException.exceptions.WaitForElementToBeLocatedException;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;

import java.util.ArrayList;
import java.util.List;

public abstract class AssertEventsPerformAbstract implements AssertEventsPerformModel {
    private final JsManager jsManager;
    private final ExecutorManager executorManager;
    protected final List<String> xpaths;
    protected final String xpath;

    protected AssertEventsPerformAbstract(JsManager jsManager, ExecutorManager executorManager, List<String> xpaths) {
        this.jsManager = jsManager;
        this.executorManager = executorManager;
        this.xpaths = xpaths;
        this.xpath = "";
    }
    protected AssertEventsPerformAbstract(JsManager jsManager, ExecutorManager executorManager, String xpath) {
        this.jsManager = jsManager;
        this.executorManager = executorManager;
        this.xpaths = new ArrayList<>();
        this.xpath = xpath;
    }
    protected WebElement getWebElement(String xpath)throws AssertExceptionAbstract {
        try {
            WebElement webElement =  jsManager.uploadCode(executorManager,"return window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.extractWebElement(arguments[0]);",xpath);
            if (webElement == null){
                throw new NullPointerException(WaitForElementToBeLocatedException.CAUSE_INVALID_SELECTOR);
            }else{
                return webElement;
            }
        }catch (WebDriverException | NullPointerException e){
            WaitForElementToBeLocatedException waitForElementToBeLocatedException = new WaitForElementToBeLocatedException(e.getMessage());
            throw waitForElementToBeLocatedException;
        }
    }
    protected WebElement getWebElement()throws AssertExceptionAbstract{
        try {
            WebElement webElement = jsManager.uploadCode(executorManager,"return window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.extractWebElement(arguments[0]);",xpath);
            if (webElement == null){
                throw new NullPointerException(WaitForElementToBeLocatedException.CAUSE_INVALID_SELECTOR);
            }else{
                return webElement;
            }
        }catch (WebDriverException | NullPointerException e){
            WaitForElementToBeLocatedException waitForElementToBeLocatedException = new WaitForElementToBeLocatedException(e.getMessage());
            throw waitForElementToBeLocatedException;
        }

    }
    protected List<WebElement> getWebElements()throws AssertExceptionAbstract{
        try {
            List<WebElement> webElements = new ArrayList<>();
            for (String path: xpaths){
                webElements.add(getWebElement(path));
            }
            return webElements;
        }catch (AssertExceptionAbstract e){
            throw e;
        }
    }

}
