package recorder.components.events.assertEventPerformModel.assertEventPerformModelImpl.codeAssertionEvent;


import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.assertEventPerformModel.AssertEventsPerformAbstract;
import recorder.components.events.assertionCodeRunner.AssertionCodeRunner;
import recorder.components.exception.assertException.AssertExceptionAbstract;

import java.util.HashMap;
import java.util.List;

public class AssertWithCode extends AssertEventsPerformAbstract {
    public static final String TYPE = "codeAssert";

    private final String codeScript;
    public AssertWithCode(JsManager jsManager, ExecutorManager executorManager, List<String> xpath, HashMap<String,Object> attributes) {
        super(jsManager, executorManager, xpath);
        this.codeScript = (String) attributes.get("code");
    }

    @Override
    public boolean assertEvent() throws AssertExceptionAbstract {
        try {
            return AssertionCodeRunner.runAssertionCode(codeScript, getWebElements());
        }catch (AssertExceptionAbstract assertExceptionAbstract){
            assertExceptionAbstract.setAssertion(codeScript);
            throw assertExceptionAbstract;
        }
    }

    @Override
    public boolean tryAssertExceptionSolution(HashMap<String, String> report, AssertExceptionAbstract assertExceptionAbstract) {
        System.out.println("ovjde sam i cekam na assert idemo dalje ahahahahahah");
        switch (assertExceptionAbstract.getTypeOfAssertException()){
            case WAIT_FOR_WEB_ELEMENT_TO_BE_LOCATED:break;
            case ASSERT_RETURN_VALUE:break;
        }
        return false;
    }
}
