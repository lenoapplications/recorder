package recorder.components.events.assertEventPerformModel.assertEventPerformModelImpl.documentAssertEvent;

import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.assertEventPerformModel.AssertEventsPerformAbstract;

import java.util.HashMap;

public class IsElementDisplayed extends AssertEventsPerformAbstract {
    public static final String TYPE = "isDisplayed";

    public IsElementDisplayed(JsManager jsManager, ExecutorManager executorManager, String xpath, HashMap<String,Object> attributes) {
        super(jsManager, executorManager, xpath);

    }

    @Override
    public boolean assertEvent() {
      return false;
    }

    @Override
    public boolean tryAssertExceptionSolution(HashMap<String, String> report, AssertExceptionAbstract assertExceptionAbstract) {
        return false;
    }
}

