package recorder.components.events.assertEventHolder;

import recorder.components.events.assertEventHolder.assertionType.AssertionType;

import java.io.Serializable;
import java.util.List;

public class SaveAssertAttributeList implements Serializable {
    private List<String> xpaths;
    private List<AssertionType> assertionTypeList;

    public List<AssertionType> getAssertionTypeList() {
        return assertionTypeList;
    }

    public void setAssertionTypeList(List<AssertionType> assertionTypeList) {
        this.assertionTypeList = assertionTypeList;
    }
    public List<String> getXpaths() {
        return xpaths;
    }

    public void setXpath(List<String> xpaths) {
        this.xpaths = xpaths;
    }
}
