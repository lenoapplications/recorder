package recorder.components.events.assertEventHolder.assertionType;



import java.io.Serializable;
import java.util.HashMap;

public class AssertionType implements Serializable {
    private String type;
    private HashMap<String,Object> attributes;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public HashMap<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, Object> attributes) {
        this.attributes = attributes;
    }
}
