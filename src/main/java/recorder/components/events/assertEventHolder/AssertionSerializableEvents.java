package recorder.components.events.assertEventHolder;


import java.io.Serializable;


public class AssertionSerializableEvents implements Serializable {
    private SaveAssertAttributeList saveAssertAttributeList;

    public SaveAssertAttributeList getSaveAssertAttributeList() {
        return saveAssertAttributeList;
    }

    public void setSaveAssertAttributeList(SaveAssertAttributeList saveAssertAttributeList) {
        this.saveAssertAttributeList = saveAssertAttributeList;
    }
}
