package recorder.components.events.eventHolder;

import recorder.components.events.assertEventHolder.AssertionSerializableEvents;

import java.io.Serializable;

public abstract class SerializableEventsDao implements Serializable {
    private final AssertionSerializableEvents assertionSerializableEvents;
    private final SerializableEventsDto serializableEventsDto;

    protected SerializableEventsDao(SerializableEventsDto serializableEventsDto) {
        this.serializableEventsDto = serializableEventsDto;
        assertionSerializableEvents = new AssertionSerializableEvents();
    }

    public AssertionSerializableEvents getAssertionSerializableEvents() {
        return assertionSerializableEvents;
    }
    public SerializableEventsDto getSerializableEventsDto(){
       return serializableEventsDto;
    }
}
