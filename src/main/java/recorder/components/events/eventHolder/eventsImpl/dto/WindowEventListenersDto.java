package recorder.components.events.eventHolder.eventsImpl.dto;

import recorder.components.events.eventHolder.SerializableEventsDto;

public class WindowEventListenersDto extends SerializableEventsDto {
    private String windowName;


    public String getWindowName() {
        return windowName;
    }

    public void setWindowName(String windowName) {
        this.windowName = windowName;
    }

}
