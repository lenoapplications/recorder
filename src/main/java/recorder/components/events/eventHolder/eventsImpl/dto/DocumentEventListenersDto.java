package recorder.components.events.eventHolder.eventsImpl.dto;

import recorder.components.events.eventHolder.SerializableEventsDto;


public class DocumentEventListenersDto extends SerializableEventsDto {
    private String xpath;
    private String value;
    private String tag;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
