package recorder.components.events.eventHolder.eventsImpl.dao;

import recorder.components.events.eventHolder.SerializableEventsDao;
import recorder.components.events.eventHolder.eventsImpl.dto.DocumentEventListenersDto;

public class DocumentEventListenerDao extends SerializableEventsDao {

    public DocumentEventListenerDao(DocumentEventListenersDto documentEventListenersDto) {
        super(documentEventListenersDto);
    }

}
