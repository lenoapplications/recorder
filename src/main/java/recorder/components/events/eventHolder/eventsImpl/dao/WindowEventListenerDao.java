package recorder.components.events.eventHolder.eventsImpl.dao;

import recorder.components.events.eventHolder.SerializableEventsDao;
import recorder.components.events.eventHolder.eventsImpl.dto.WindowEventListenersDto;


public class WindowEventListenerDao extends SerializableEventsDao {

    public WindowEventListenerDao(WindowEventListenersDto windowEventListenersDto) {
        super(windowEventListenersDto);
    }

}
