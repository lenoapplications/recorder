package recorder.components.events.eventHolder;


import java.io.Serializable;

public abstract class SerializableEventsDto implements Serializable {
    private String event;
    private String eventOn;

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEvent() {
        return event;
    }

    public String getEventOn() {
        return eventOn;
    }

    public void setEventOn(String eventOn) {
        this.eventOn = eventOn;
    }

}
