package recorder.components.exception.assertException.exceptions;

import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.assertException.exceptionsEnum.AssertExceptionsEnum;
import recorder.components.webRecordedElement.RecordedAssertModel;

import java.util.HashMap;

public class AssertReturnValueException extends AssertExceptionAbstract {

    public AssertReturnValueException(){
        super();
    }

    @Override
    public void makeInfoReport(HashMap<String, String> report) {
        report.put("Assertion",assertion);
        report.put("Reason","Assertion return null");
    }


    @Override
    public void solutionSuccess(HashMap<String, String> report) {
        report.put("Solution success",solutionMessage);
    }

    @Override
    public void solutionFailed(HashMap<String, String> report) {
        report.put("Solution failed",solutionMessage);
    }

    @Override
    public AssertExceptionsEnum getTypeOfAssertException() {
        return AssertExceptionsEnum.ASSERT_RETURN_VALUE;
    }
}
