package recorder.components.exception.assertException.exceptions;

import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.assertException.exceptionsEnum.AssertExceptionsEnum;

import java.util.HashMap;

public class WaitForElementToBeLocatedException extends AssertExceptionAbstract {
    public static final String CAUSE_INVALID_SELECTOR = "invalid selector";

    private final String causeOfTimeout;

    public WaitForElementToBeLocatedException(String causeOfTimeout){
        this.causeOfTimeout = parseCauseOfTimeout(causeOfTimeout);
        System.out.println(this.causeOfTimeout);
    }

    public String getCauseOfTimeout() {
        return causeOfTimeout;
    }


    @Override
    public void makeInfoReport(HashMap<String, String> report) {
        report.put("Assertion",assertion);
        report.put("Reason","Element was not located in 30 sec");
        report.put("Cause of timeout",causeOfTimeout);
    }

    @Override
    public void solutionSuccess(HashMap<String, String> report) {
        report.put("Solution success","Element located");
    }

    @Override
    public void solutionFailed(HashMap<String, String> report) {
        report.put("Solution failed","Element not located");

    }

    private String parseCauseOfTimeout(String causeOfTimeout){
        if (causeOfTimeout.indexOf("\n") == -1){
            causeOfTimeout = causeOfTimeout.concat("\n");
        }
        String parsedCauseOfTimeout = causeOfTimeout.substring(0,causeOfTimeout.indexOf("\n"));
        return parsedCauseOfTimeout;
    }

    @Override
    public AssertExceptionsEnum getTypeOfAssertException() {
        return AssertExceptionsEnum.WAIT_FOR_WEB_ELEMENT_TO_BE_LOCATED;
    }
}
