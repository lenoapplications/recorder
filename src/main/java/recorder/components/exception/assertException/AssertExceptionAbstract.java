package recorder.components.exception.assertException;

import recorder.components.webRecordedElement.RecordedAssertModel;

public abstract class AssertExceptionAbstract extends Exception implements AssertExceptionModel{
    protected String solutionMessage;
    protected String assertion;
    protected RecordedAssertModel recordedAssertModel;

    protected AssertExceptionAbstract(String assertion) {
        this.recordedAssertModel = recordedAssertModel;
        this.assertion = assertion;
        this.solutionMessage = "Solution not provided";
    }
    protected AssertExceptionAbstract(){
        this.solutionMessage = "Solution not provided";
    }

    public String getAssertion() {
        return assertion;
    }


    public void setAssertion(String assertion) {
        this.assertion = assertion;
    }

    public RecordedAssertModel getRecordedAssertModel() {
        return recordedAssertModel;
    }
    public void setRecordedAssertModel(RecordedAssertModel recordedAssertModel) {
        this.recordedAssertModel = recordedAssertModel;
    }
}
