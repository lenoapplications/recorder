package recorder.components.exception.assertException;

import recorder.components.exception.abstractException.ExceptionModel;

import java.util.ArrayList;
import java.util.List;

public class AssertException extends ExceptionModel {
    private final List<AssertExceptionAbstract> assertExceptionAbstractList;

    public AssertException() {
        super();
        assertExceptionAbstractList = new ArrayList<>();
    }

    @Override
    public void makeReport() {
        if (webRecordedElement != null){
            report.put("Event",webRecordedElement.getSerializableEventsDto().getEvent());
            report.put("Type exception","Assert");
            report.put("Number of exceptions", String.valueOf(assertExceptionAbstractList.size()));
        }
    }

    @Override
    public boolean provideSolution() {
        for (AssertExceptionAbstract assertExceptionAbstract : assertExceptionAbstractList){
            if (!assertExceptionAbstractSolution(assertExceptionAbstract)){
                return false;
            }
        }
        return true;
    }


    public List<AssertExceptionAbstract> getAssertExceptionAbstracts() {
        return assertExceptionAbstractList;
    }
    public void addAssertExceptionAbstract(AssertExceptionAbstract assertExceptionAbstract){
        assertExceptionAbstractList.add(assertExceptionAbstract);
    }
    public boolean isAssertSuccessful(){
        return assertExceptionAbstractList.size() == 0;
    }


    private boolean assertExceptionAbstractSolution(AssertExceptionAbstract assertExceptionAbstract){
        if (webRecordedElement.assertExceptionSolution(report,assertExceptionAbstract)){
            return true;
        }else{
            return false;
        }
    }
}
