package recorder.components.exception.assertException.exceptionsEnum;

public enum AssertExceptionsEnum {
    ASSERT_RETURN_VALUE,
    WAIT_FOR_WEB_ELEMENT_TO_BE_LOCATED,
}
