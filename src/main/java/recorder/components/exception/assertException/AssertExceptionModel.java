package recorder.components.exception.assertException;

import recorder.components.exception.abstractException.ConcreteExceptionModel;
import recorder.components.exception.assertException.exceptionsEnum.AssertExceptionsEnum;


public interface AssertExceptionModel extends ConcreteExceptionModel {
    AssertExceptionsEnum getTypeOfAssertException();
}
