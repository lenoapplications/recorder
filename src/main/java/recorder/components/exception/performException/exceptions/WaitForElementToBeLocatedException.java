package recorder.components.exception.performException.exceptions;

import recorder.components.events.eventsPerformModel.EventsPerformAbstract;
import recorder.components.exception.performException.PerformExceptionModel;
import recorder.components.exception.performException.exceptionsEnum.PerformExceptionsEnum;

import java.util.HashMap;

public class WaitForElementToBeLocatedException implements PerformExceptionModel {
    public static final String CAUSE_INVALID_SELECTOR = "invalid selector";
    public static final String FLOWMOVERECORDER_UNDEFINED = "Cannot read property 'flowMoveFunctionsRecorder' of undefined";

    private final String causeOfTimeout;

    public WaitForElementToBeLocatedException(String causeOfTimeout){
        this.causeOfTimeout = parseCauseOfTimeout(causeOfTimeout);
    }

    public String getCauseOfTimeout() {
        return causeOfTimeout;
    }

    @Override
    public PerformExceptionsEnum getTypeOfPerformException() {
        return PerformExceptionsEnum.WAIT_FOR_WEB_ELEMENT_TO_BE_LOCATED;
    }

    @Override
    public void makeInfoReport(HashMap<String, String> report) {
        report.put("Reason","Element was not located in 30 sec");
        report.put("Cause of timeout",causeOfTimeout);
    }

    @Override
    public void solutionSuccess(HashMap<String, String> report) {
        report.put("Solution success","Element located");
    }

    @Override
    public void solutionFailed(HashMap<String, String> report) {
        report.put("Solution failed","Element not located");

    }
    @Override
    public boolean solve(EventsPerformAbstract eventsPerformAbstract,HashMap<String,String> report){
        if (eventsPerformAbstract.isDocumentComplete()){
            return checkIfCauseIsNotFixable(report,eventsPerformAbstract);
        }else{
            report.put("Document status","document not loaded completely");
            return false;
        }
    }

    private String parseCauseOfTimeout(String causeOfTimeout){
        if (causeOfTimeout.indexOf("\n") == - 1){
            causeOfTimeout = causeOfTimeout.concat("\n");
        }
        String parsedCauseOfTimeout = causeOfTimeout.substring(0,causeOfTimeout.indexOf("\n"));
        return parsedCauseOfTimeout;
    }

    private boolean checkIfCauseIsNotFixable(HashMap<String,String> report,EventsPerformAbstract eventsPerformAbstract){
        if (causeOfTimeout.contains(WaitForElementToBeLocatedException.CAUSE_INVALID_SELECTOR)){
            report.put("Cause of timeout","selector is invalid");
            return false;
        }else if (causeOfTimeout.contains(WaitForElementToBeLocatedException.FLOWMOVERECORDER_UNDEFINED)){
            eventsPerformAbstract.refreshJsCode();
            return true;
        }
        return true;
    }
}
