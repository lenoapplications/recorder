package recorder.components.exception.performException;

import recorder.components.events.eventsPerformModel.EventsPerformAbstract;
import recorder.components.exception.abstractException.ConcreteExceptionModel;
import recorder.components.exception.performException.exceptionsEnum.PerformExceptionsEnum;

import java.util.HashMap;


public interface PerformExceptionModel extends ConcreteExceptionModel {
    PerformExceptionsEnum getTypeOfPerformException();
    boolean solve(EventsPerformAbstract eventsPerformAbstract,HashMap<String,String> report);
}
