package recorder.components.exception.performException;

import recorder.components.exception.abstractException.ExceptionModel;


public class PerformException extends ExceptionModel {
    private final PerformExceptionModel performExceptionModel;

    public PerformException(PerformExceptionModel performExceptionModel){
        super();
        this.performExceptionModel = performExceptionModel;
    }

    @Override
    public void makeReport() {
        if (webRecordedElement != null){
            report.put("Event",webRecordedElement.getSerializableEventsDto().getEvent());
            report.put("Type exception","Perform");
        }
    }

    @Override
    public boolean provideSolution() {
        if (webRecordedElement.performExceptionSolution(report,performExceptionModel)){
            performExceptionModel.solutionSuccess(report);
            return true;
        }else{
            performExceptionModel.solutionFailed(report);
            return false;
        }
    }


}
