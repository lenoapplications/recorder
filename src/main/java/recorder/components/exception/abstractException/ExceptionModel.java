package recorder.components.exception.abstractException;

import recorder.components.exception.abstractException.exceptionSolutioner.ExceptionSolutioner;
import recorder.components.webRecordedElement.WebRecordedElement;

public abstract class ExceptionModel extends ExceptionSolutioner {
    protected WebRecordedElement webRecordedElement;

    protected ExceptionModel(){
        super();
    }

    public WebRecordedElement getWebRecordedElement() {
        return webRecordedElement;
    }

    public void setWebRecordedElement(WebRecordedElement webRecordedElement) {
        this.webRecordedElement = webRecordedElement;
    }

}
