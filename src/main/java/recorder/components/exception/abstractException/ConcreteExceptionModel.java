package recorder.components.exception.abstractException;


import java.util.HashMap;

public interface ConcreteExceptionModel{
    void solutionSuccess(HashMap<String,String> report);
    void solutionFailed(HashMap<String,String> report);
    void makeInfoReport(HashMap<String,String> report);
}
