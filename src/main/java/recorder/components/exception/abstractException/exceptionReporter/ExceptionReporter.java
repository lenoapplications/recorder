package recorder.components.exception.abstractException.exceptionReporter;

import java.util.HashMap;

public abstract class ExceptionReporter extends Exception implements ReporterInterface{
    protected final HashMap<String,String> report;

    protected ExceptionReporter(){
        this.report = new HashMap<>();
    }

    public HashMap<String,String> getReport() {
        makeReport();
        return report;
    }
}
