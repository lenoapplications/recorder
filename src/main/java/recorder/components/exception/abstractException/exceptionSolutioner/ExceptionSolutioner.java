package recorder.components.exception.abstractException.exceptionSolutioner;

import recorder.components.exception.abstractException.exceptionReporter.ExceptionReporter;

public abstract class ExceptionSolutioner extends ExceptionReporter implements SolutionerInterface{

    protected ExceptionSolutioner(){
        super();
    }

}
