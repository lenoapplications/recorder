package recorder.components.exception.abstractException.exceptionSolutioner;

public interface SolutionerInterface {
    boolean provideSolution();
}
