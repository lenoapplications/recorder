package recorder.components.javascript.executorModel;

import org.openqa.selenium.JavascriptExecutor;

public interface ExecutorModel {
    default void execute(JavascriptExecutor javascriptExecutor) {

    }
    default Object executeAndReturn(JavascriptExecutor javascriptExecutor){
        return null;
    }
}
