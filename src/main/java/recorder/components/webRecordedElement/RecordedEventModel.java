package recorder.components.webRecordedElement;

import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.events.eventsPerformModel.EventsPerformModel;
import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.performException.PerformException;
import recorder.components.exception.performException.PerformExceptionModel;

import java.util.HashMap;

public class RecordedEventModel<EVENT extends EventsPerformModel> {

    EVENT eventPerform;

    public void setEventPerform(EVENT eventPerform) {
        this.eventPerform = eventPerform;
    }

    public void perform(SerializableEventsDto serializableEvents)throws PerformException {
        eventPerform.perform(serializableEvents);
    }
    public boolean performExceptionSolution(HashMap<String,String> report, SerializableEventsDto serializableEventsDto, PerformExceptionModel performExceptionModel){
        return eventPerform.tryPerformExceptionSolution(report,serializableEventsDto,performExceptionModel);
    }
}
