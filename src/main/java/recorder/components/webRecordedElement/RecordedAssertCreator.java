package recorder.components.webRecordedElement;

import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.assertEventHolder.assertionType.AssertionType;
import recorder.components.events.assertEventPerformModel.AssertEventsPerformModel;
import recorder.components.events.assertEventPerformModel.assertEventPerformModelImpl.codeAssertionEvent.AssertWithCode;

import java.util.ArrayList;
import java.util.List;

public class RecordedAssertCreator {

    public static List<RecordedAssertModel> createRecordedAssertModel(List<String> xpaths,List<AssertionType> assertionTypeList, JsManager jsManager, ExecutorManager executorManager){
        List<RecordedAssertModel> list = new ArrayList<>();
        for (AssertionType assertionType : assertionTypeList){
            switch (assertionType.getType()){
                case AssertWithCode.TYPE:list.add(createAssertWithCodeRecordedAssertModel(xpaths,assertionType,jsManager,executorManager));break;
            }
        }
        return list;
    }


    private static RecordedAssertModel<AssertWithCode> createAssertWithCodeRecordedAssertModel(List<String> xpaths,AssertionType assertionType,JsManager jsManager,ExecutorManager executorManager){
        AssertWithCode assertWithCode = new AssertWithCode(jsManager,executorManager,xpaths,assertionType.getAttributes());
        return create(assertWithCode);

    }


    private static <A extends AssertEventsPerformModel> RecordedAssertModel<A> create(A eventModel){
        RecordedAssertModel<A> recordedEventModel = new RecordedAssertModel<>();
        recordedEventModel.setAssertPerform(eventModel);
        return recordedEventModel;
    }
}
