package recorder.components.webRecordedElement;

import recorder.components.events.assertEventPerformModel.AssertEventsPerformModel;
import recorder.components.exception.assertException.AssertExceptionAbstract;

import java.util.HashMap;

public class RecordedAssertModel <EVENT extends AssertEventsPerformModel>{

    private EVENT assertPerform;

    public void setAssertPerform(EVENT assertPerform){
        this.assertPerform = assertPerform;
    }
    public void perform()throws AssertExceptionAbstract {
        assertPerform.assertEvent();
    }
    public boolean assertExceptionSolution(HashMap<String,String> report,AssertExceptionAbstract assertExceptionAbstract){
        return assertPerform.tryAssertExceptionSolution(report,assertExceptionAbstract);
    }
}
