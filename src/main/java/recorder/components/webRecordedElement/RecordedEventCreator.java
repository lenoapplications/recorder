package recorder.components.webRecordedElement;

import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.events.eventsPerformModel.EventsPerformModel;
import recorder.components.events.eventsPerformModel.eventsPerformModelImpl.documentsEvents.Click;
import recorder.components.events.eventsPerformModel.eventsPerformModelImpl.documentsEvents.Input;
import recorder.components.events.eventsPerformModel.eventsPerformModelImpl.windowEvents.UnloadEvent;

public class RecordedEventCreator {
    public static RecordedEventModel createRecordedEventModel(String event, String eventOn, DriverManager driverManager, JsManager jsManager, ExecutorManager executorManager){
        if (eventOn.equals("document")){
            return eventOnDocument(event,driverManager,jsManager,executorManager);
        }else{
            return eventOnWindow(event,driverManager,jsManager,executorManager);
        }
    }

    private static RecordedEventModel eventOnDocument(String event,DriverManager driverManager,JsManager jsManager,ExecutorManager executorManager ){
        switch (event){
            case "click":return createClickEvent(driverManager,jsManager,executorManager);
            case "input":return createInputEvent(driverManager,jsManager,executorManager);
            default:{
                return new RecordedEventModel();
            }
        }
    }

    private static RecordedEventModel eventOnWindow(String event,DriverManager driverManager,JsManager jsManager,ExecutorManager executorManager){
        switch (event){
            case "beforeunload":return createRefreshJsCodeEvent(driverManager,jsManager,executorManager);
            default:{
                return new RecordedEventModel();
            }
        }
    }

    private static RecordedEventModel<UnloadEvent> createRefreshJsCodeEvent(DriverManager driverManager,JsManager jsManager,ExecutorManager executorManager){
        UnloadEvent unloadEvent = new UnloadEvent(driverManager,jsManager,executorManager);
        return create(unloadEvent);
    }

    private static RecordedEventModel<Click> createClickEvent(DriverManager driverManager,JsManager jsManager,ExecutorManager executorManager){
        Click click = new Click(driverManager,jsManager,executorManager);
        return create(click);
    }

    private static RecordedEventModel<Input> createInputEvent(DriverManager driverManager,JsManager jsManager,ExecutorManager executorManager){
        Input input = new Input(driverManager,jsManager,executorManager);
        return create(input);
    }


    private static <A extends EventsPerformModel> RecordedEventModel<A> create(A eventModel){
        RecordedEventModel<A> recordedEventModel = new RecordedEventModel<>();
        recordedEventModel.setEventPerform(eventModel);
        return recordedEventModel;
    }
}
