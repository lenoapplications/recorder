package recorder.components.webRecordedElement;

import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.exception.abstractException.ExceptionModel;
import recorder.components.exception.assertException.AssertException;
import recorder.components.exception.assertException.AssertExceptionAbstract;
import recorder.components.exception.performException.PerformException;
import recorder.components.exception.performException.PerformExceptionModel;

import java.util.HashMap;
import java.util.List;

public class WebRecordedElement {
    private final SerializableEventsDto serializableEventsDto;
    private final RecordedEventModel recordedEventModel;
    private final List<RecordedAssertModel> recordedAssertModel;

    public WebRecordedElement(SerializableEventsDto serializableEventHolder, RecordedEventModel recordedEventModel,List<RecordedAssertModel> recordedAssertModel) {
        this.serializableEventsDto = serializableEventHolder;
        this.recordedEventModel = recordedEventModel;
        this.recordedAssertModel = recordedAssertModel;
    }

    public void perform() throws ExceptionModel {
        performRecordEvent();
        performAssertsEvents();
    }
    public boolean performExceptionSolution(HashMap<String,String> report, PerformExceptionModel performException){
        return recordedEventModel.performExceptionSolution(report,serializableEventsDto,performException);
    }
    public boolean assertExceptionSolution(HashMap<String,String> report, AssertExceptionAbstract assertExceptionAbstract){
        return assertExceptionAbstract.getRecordedAssertModel().assertExceptionSolution(report,assertExceptionAbstract);
    }
    public SerializableEventsDto getSerializableEventsDto() {
        return serializableEventsDto;
    }




    private void performRecordEvent() throws PerformException {
        try {
            this.recordedEventModel.perform(this.serializableEventsDto);
        } catch (PerformException exceptionModel) {
            throw exceptionModel;
        }
    }
    private void performAssertsEvents() throws AssertException {
        AssertException mainAssertException = new AssertException();
        for (RecordedAssertModel assertModel : recordedAssertModel){
            try {
                assertModel.perform();
            }catch (AssertExceptionAbstract exceptionAbstract){
                exceptionAbstract.setRecordedAssertModel(assertModel);
                mainAssertException.addAssertExceptionAbstract(exceptionAbstract);
            }
        }
        if (! mainAssertException.isAssertSuccessful()){
            throw mainAssertException;
        }

    }
}
