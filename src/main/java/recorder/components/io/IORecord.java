package recorder.components.io;

import recorder.managers.configManager.ConfigManager;
import recorder.components.configuration.properties.ConfigurationProperties;

import java.io.*;

public abstract class IORecord{
    private String mainRecordLocation;
    private File currentActiveRecord;

    protected IORecord() {
        String location = ConfigManager.getConfigurationParameter(ConfigurationProperties.RECORD_STORAGE_PATH);
        checkIfAllValid(location);
    }

    private void checkIfAllValid(String location){
        mainRecordLocation = checkLocationFormat(location);
        checkIfDirExists();
    }
    private String checkLocationFormat(String location){
        if (location.endsWith("/")){
            return location;
        }else{
            return location.concat("/");
        }
    }
    private void checkIfDirExists(){
        File dirLocation = new File(mainRecordLocation);
        if (!dirLocation.exists() || !dirLocation.isDirectory()){
            dirLocation.mkdir();
        }
    }


    protected String addFileExstension(String recordName){
        return recordName.concat(".rct");
    }

    protected boolean createFile(String fileName){
        try {
            return new File(mainRecordLocation.concat(fileName)).createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    protected File open(String fileName){
        currentActiveRecord = new File(mainRecordLocation.concat(fileName));
        return currentActiveRecord;
    }

    protected void closeCurrentActiveRecord(){
        currentActiveRecord = null;
    }


}
