package recorder.components.records.recordHolder;

import recorder.components.events.eventHolder.SerializableEventsDao;
import java.util.ArrayList;

public class RecordHolder {
    private final ArrayList<SerializableEventsDao> serializableEventHolders;

    public RecordHolder() {
        serializableEventHolders = new ArrayList<>();
    }
    public void addNewEvent(SerializableEventsDao serializableEventHolder){
        this.serializableEventHolders.add(serializableEventHolder);
    }

    public ArrayList<SerializableEventsDao> getSerializableEventHolders() {
        return serializableEventHolders;
    }
}
