package recorder.components.records.format;

public enum Events {
    MOUSE_LEFT_CLICK("MOUSE_LFT_CLICK"),
    MOUSE_MOVE("MOUSE_MOVE");


    public String event;

    Events(String event){
        this.event = event;
    }
}
