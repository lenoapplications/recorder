package recorder.components.records.format;

public enum EventFormat {

    EVENT_RECORD_BASE_MOUSE_FORMAT("%s_(%s:%s)\n");


    private final String format;


    EventFormat(String format) {
        this.format = format;
    }


    public static String fillBaseMouseFormat(String eventName,int xPosition,int yPosition){
        return String.format(EVENT_RECORD_BASE_MOUSE_FORMAT.format,eventName,xPosition,yPosition);
    }
}
