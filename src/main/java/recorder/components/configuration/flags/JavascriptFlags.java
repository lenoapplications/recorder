package recorder.components.configuration.flags;

public enum JavascriptFlags {
    IS_RECORDING("isRecording","$<isRecording>"),
    IS_ASSERTION_ACTIVATED("isAssertionActivated","$<isAssertionActivated>")

    ;

    private String parameterSyntax;
    private String property;

    JavascriptFlags(String property,String parameterSyntax){
        this.parameterSyntax = parameterSyntax;
        this.property = property;
    }

    public String getParameterSyntax() {
        return parameterSyntax;
    }

    public String getProperty() {
        return property;
    }

    @Override
    public String toString() {
        return property;
    }
}
