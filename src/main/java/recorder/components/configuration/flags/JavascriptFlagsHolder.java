package recorder.components.configuration.flags;

import java.util.HashMap;

public class JavascriptFlagsHolder {
    private final HashMap<String,Boolean> flags;

    public JavascriptFlagsHolder() {
        flags = new HashMap<>();
    }

    public void init(){
        for(JavascriptFlags flag : JavascriptFlags.values()){
            flags.put(flag.getProperty(),false);
        }
    }
    public void changeValue(String flag,Boolean value){
        flags.put(flag,value);
    }
    public Boolean getValue(String flag){
        return flags.get(flag);
    }

    public HashMap<String, Boolean> getFlags() {
        return flags;
    }

    public void reset(){
        flags.clear();
        init();
    }
}
