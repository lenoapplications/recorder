package recorder.components.configuration.properties;

public enum JavascriptConfigurationProperties {

    SERVER_RECORDER_URL("SERVER_RECORDER_URL","$<url>"),
    ACTIVE_WINDOW_NAME("<NOT_CONFIGURABLE>","$<currentActiveWindowName>")
    ;

    private String parameterSyntax;
    private String property;

    JavascriptConfigurationProperties(String property,String parameterSyntax){
        this.parameterSyntax = parameterSyntax;
        this.property = property;
    }

    public String getParameterSyntax() {
        return parameterSyntax;
    }

    public String getProperty() {
        return property;
    }

    @Override
    public String toString() {
        return property;
    }
}
