package recorder.components.configuration.properties;

public enum ConfigurationProperties {
    RECORD_STORAGE_PATH("RECORD_STORAGE_PATH"),
    WEB_DRIVERS_LOCATION("WEB_DRIVERS_LOCATION"),
    DRIVER("DRIVER"),
    BROWSER("BROWSER"),
    WEB_URL("WEB_URL"),



    FIREFOX_DRIVER("firefox"),
    CHROME_DRIVER("chrome");


    private String property;

    ConfigurationProperties(String property){
        this.property = property;
    }


    @Override
    public String toString() {
        return property;
    }
}
