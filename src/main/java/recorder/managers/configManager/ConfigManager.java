package recorder.managers.configManager;

import recorder.components.configuration.properties.ConfigurationProperties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigManager {
    private static String resourcePath = "./src/main/resources/";

    private static ConfigManager configManager;

    public static boolean initConfig(){
        try {
            if ( configManager == null) {
                configManager = new ConfigManager();
                configManager.readFromProperties();
                return true;
            }else{
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getConfigurationParameter(ConfigurationProperties property){
        return configManager.configuration.get(property.toString());
    }


    private final Properties properties;
    private Map<String,String> configuration;

    private ConfigManager(){
        properties = new Properties();
        configuration = new HashMap<>();
    }


    private void readFromProperties() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(resourcePath.concat("configuration.properties"));
        properties.load(fileInputStream);
        for (ConfigurationProperties property : ConfigurationProperties.values()){
            configuration.put(property.toString(),properties.getProperty(property.toString()));
        }
    }

}
