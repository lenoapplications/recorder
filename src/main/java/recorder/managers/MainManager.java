package recorder.managers;

import recorder.managers.configManager.ConfigManager;
import recorder.managers.recordsManagers.RecordsManager;
import recorder.managers.recordsManagers.recordRunner.RecordRunnerState;
import recorder.managers.seleniumManager.SeleniumManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.components.records.recordHolder.RecordHolder;

public class MainManager {

    private final RecordsManager recordsManager;
    private final SeleniumManager seleniumManager;

    public MainManager() {
        ConfigManager.initConfig();
        DriverManager driverManager = new DriverManager();
        this.recordsManager = new RecordsManager(driverManager);
        this.seleniumManager = new SeleniumManager(driverManager);
    }

    public RecordHolder startNewTestRecord(String fileName){
        seleniumManager.initializationForRecording();
        return recordsManager.prepareNewTestRecord();
    }
    public RecordHolder startNewTestRecord(String url,String choosedDriver){
        seleniumManager.initializationForRecording(url,choosedDriver);
        return recordsManager.prepareNewTestRecord();
    }

    public void saveTestRecord(String fileName){
        recordsManager.saveTestRecord(fileName);
        recordsManager.closeTestRecord();
    }

    public RecordRunnerState runTestRecord(String filename){
        return recordsManager.startTestRecord(seleniumManager,filename);
    }


    public SeleniumManager getSeleniumManager() {
        return seleniumManager;
    }
}
