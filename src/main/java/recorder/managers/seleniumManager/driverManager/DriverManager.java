package recorder.managers.seleniumManager.driverManager;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;
import recorder.managers.configManager.ConfigManager;
import recorder.components.configuration.properties.ConfigurationProperties;
import recorder.components.selenium.base.driverContext.DriverContext;

import java.util.Set;

public class DriverManager {
    private DriverContext driverContext;


    public void initDriver(){
        WebDriver driver;
        String choosedDriver = ConfigManager.getConfigurationParameter(ConfigurationProperties.BROWSER).toLowerCase();
        String driverLocation = ConfigManager.getConfigurationParameter(ConfigurationProperties.WEB_DRIVERS_LOCATION);

        if ((driver = createDriver(choosedDriver,driverLocation))!= null){
            driverContext = new DriverContext(driver);
        }
    }
    public void initDriver(String choosedDriver){
        WebDriver driver;
        String driverLocation = ConfigManager.getConfigurationParameter(ConfigurationProperties.WEB_DRIVERS_LOCATION);

        if ((driver = createDriver(choosedDriver,driverLocation))!= null){
            driverContext = new DriverContext(driver);
        }
    }

    public void startWebDriver(){
        driverContext.getEventFiringWebDriver().navigate().to(ConfigManager.getConfigurationParameter(ConfigurationProperties.WEB_URL));
    }
    public void startWebDriver(String url){
        driverContext.getEventFiringWebDriver().navigate().to(url);
    }

    public void closeWebDriver(){
        driverContext.getDriver().close();
    }

    public WebDriver.Window getWindow(){
        return driverContext.getDriver().manage().window();
    }
    public Set<String> getWindowNames(){
        return driverContext.getDriver().getWindowHandles();
    }
    public String getWindowName(){
        return driverContext.getDriver().getWindowHandle();
    }
    public JavascriptExecutor getJavascriptExecutor(){
        return (JavascriptExecutor) driverContext.getDriver();
    }
    public TakesScreenshot getTakesScreenShot(){
        return (TakesScreenshot)driverContext.getDriver();
    }
    public LocalStorage getLocalStorage(){
        WebStorage webStorage = (WebStorage) new Augmenter().augment(driverContext.getDriver());
        LocalStorage localStorage = webStorage.getLocalStorage();
        return localStorage;
    }
    public WebDriver getWebDriver() {
        return driverContext.getDriver();
    }




    private WebDriver createDriver(String choosedDriver, String driverLocation){
        if (choosedDriver.contains(ConfigurationProperties.FIREFOX_DRIVER.toString())){
            System.setProperty("webdriver.gecko.driver",driverLocation.concat(choosedDriver));
            return new FirefoxDriver();
        }else if (choosedDriver.contains(ConfigurationProperties.CHROME_DRIVER.toString())){
            System.setProperty("webdriver.chrome.driver",driverLocation.concat(choosedDriver));
            return new ChromeDriver();
        }else{
            return null;
        }
    }



}
