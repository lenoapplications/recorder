package recorder.managers.seleniumManager.executorManager;

import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.components.javascript.executorModel.ExecutorModel;

public class ExecutorManager {
    private DriverManager executor;

    public ExecutorManager(){

    }
    public void setExecutor(DriverManager driverManager) {
        this.executor = driverManager;
    }

    public void executeJsCode(ExecutorModel executorModel){
        executorModel.execute(executor.getJavascriptExecutor());
    }
    public Object executeJsCodeAndReturn(ExecutorModel executorModel){
        return executorModel.executeAndReturn(executor.getJavascriptExecutor());
    }

}
