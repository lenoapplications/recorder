package recorder.managers.seleniumManager.utilManager;

import org.openqa.selenium.WebElement;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.managers.seleniumManager.utilManager.screenShotManager.ScreenShotManager;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class UtilManager {
    private final ScreenShotManager screenShotManager;
    private final ExecutorManager executorManager;
    private final JsManager jsManager;

    public UtilManager(DriverManager driverManager, ExecutorManager executorManager, JsManager jsManager) {
        this.screenShotManager = new ScreenShotManager(driverManager);
        this.executorManager = executorManager;
        this.jsManager = jsManager;
    }
    //SCREENSHOOT MANAGER
    public BufferedImage getWebScreenShotBufferedImage(){
        return screenShotManager.getScreenShotBufferedImage();
    }
    public byte[] getBufferedImageToByte(BufferedImage bufferedImage){
        return screenShotManager.getBufferedImageToByte(bufferedImage);
    }
    public byte[] getWebElementScreenShot(WebElement webElement) throws IOException {
        return screenShotManager.getScreenShotOfWebElement(webElement);
    }
    public byte[] getScreenShotWithMarkedWebElement(BufferedImage bufferedImage,WebElement webElement){
        return screenShotManager.markWebElementOnImageAndReturnByteArray(bufferedImage,webElement);
    }
    public byte[] getScreenShotWithMarkedWebElement(WebElement webElement) throws IOException {
       return screenShotManager.getScreenShotWithMarkedOfWebElement(webElement);
    }




}
