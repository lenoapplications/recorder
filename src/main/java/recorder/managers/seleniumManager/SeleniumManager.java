package recorder.managers.seleniumManager;


import recorder.components.webRecordedElement.*;
import recorder.managers.configManager.ConfigManager;
import recorder.managers.jsManager.JsManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.managers.seleniumManager.utilManager.UtilManager;
import recorder.components.configuration.flags.JavascriptFlags;
import recorder.components.configuration.properties.ConfigurationProperties;
import recorder.components.events.assertEventHolder.SaveAssertAttributeList;
import recorder.components.events.eventHolder.SerializableEventsDao;
import recorder.components.events.eventHolder.SerializableEventsDto;
import recorder.components.selenium.seleniumStatusHolder.SeleniumStatusHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SeleniumManager {
    private final DriverManager driverManager;
    private final JsManager jsManager;
    private final ExecutorManager executorManager;
    private final SeleniumStatusHolder seleniumStatusHolder;

    private UtilManager utilManager;

    public SeleniumManager(DriverManager driverManager) {
        this.driverManager = driverManager;
        this.jsManager = new JsManager();
        this.executorManager = new ExecutorManager();
        this.seleniumStatusHolder = new SeleniumStatusHolder();
        this.utilManager = new UtilManager(driverManager,executorManager,jsManager);
    }

    public void initializationForRecording(){
        seleniumStatusHolder.setCurrentURL(ConfigManager.getConfigurationParameter(ConfigurationProperties.WEB_URL));
        String windowName = openBrowser();
        initJsCode(windowName);
    }
    public void initializationForRecording(String url,String choosedDriver){
        seleniumStatusHolder.setCurrentURL(url);
        String windowName = openBrowser(choosedDriver,url);
        initJsCode(windowName);
    }


    public ArrayList<WebRecordedElement> initializationForRunningTestRecord(ArrayList<SerializableEventsDao> serializableEventHolders){
        String windowName = openBrowser();
        initJsCode(windowName);
        return createListOfWebRecordedElements(serializableEventHolders);
    }

    public ArrayList<WebRecordedElement> initializationForRunningTestRecord(String choosedDriver,String url,ArrayList<SerializableEventsDao> serializableEventHolders){
        String windowName = openBrowser(choosedDriver,url);
        initJsCode(windowName);
        return createListOfWebRecordedElements(serializableEventHolders);
    }


    public <A> A uploadJsCode(String codeToUpload,Object...args){
        return jsManager.uploadCode(this.executorManager,codeToUpload,args);
    }

    public void reuploadMainJsCode(){
        this.jsManager.uploadCompleteCode(this.executorManager);
    }
    public void changeFlagInCode(String flag,Boolean value){
        this.jsManager.changeFlagInCode(executorManager,flag,value);
    }

    public void assertionActivated(){
        this.jsManager.changeFlagInCode(executorManager,JavascriptFlags.IS_ASSERTION_ACTIVATED.getProperty(),true);
        this.jsManager.uploadAssertionActivated(executorManager);
    }
    public void assertionDeactivated(){
        this.jsManager.changeFlagInCode(executorManager,JavascriptFlags.IS_ASSERTION_ACTIVATED.getProperty(),false);
        this.jsManager.uploadAssertionDeactivated(executorManager);
    }
    public <A> A callUserApiFunction(String srcFile,String functionTag,String caller,Object...args){
        return this.jsManager.uploadUserApiCode(executorManager,srcFile,functionTag,caller,args);
    }

    public void waitForPageToReload() {
        waitForUnload();
        while(!seleniumStatusHolder.getPageRefreshingConditionStates().isCodeReadyForRefresh()){
            if (jsManager.uploadRefreshCheckerCode(executorManager)){
                seleniumStatusHolder.getPageRefreshingConditionStates().codeReadyForRefresh();
            }
        }
        seleniumStatusHolder.getPageRefreshingConditionStates().reset();
    }

    public void closeBrowser(){
        this.driverManager.closeWebDriver();
    }


    public boolean checkIfWindowClosed(String windowName){
        Set<String> activeWindows = driverManager.getWindowNames();
        for(String activeWindow : activeWindows ){
            if (activeWindow.equals(windowName)){
                return false;
            }
        }
        return true;
    }
    public UtilManager getUtilManager(){
        return utilManager;
    }



    private void initJsCode(String windowName){
        jsManager.initializeJavascriptCodes(windowName);
        jsManager.uploadCompleteCode(this.executorManager);
    }

    private String openBrowser(){
        driverManager.initDriver();
        driverManager.startWebDriver();
        this.executorManager.setExecutor(driverManager);
        return driverManager.getWindowName();
    }
    private String openBrowser(String choosedDriver,String url){
        driverManager.initDriver(choosedDriver);
        driverManager.startWebDriver(url);
        this.executorManager.setExecutor(driverManager);
        return driverManager.getWindowName();
    }

    private void waitForUnload(){
        boolean status = false;
        while(!status){
            status = jsManager.uploadWindowUnloadCheckerCode(executorManager);
        }
    }
    private ArrayList<WebRecordedElement> createListOfWebRecordedElements(ArrayList<SerializableEventsDao> serializableEventHolders){
        ArrayList<WebRecordedElement> webRecordedElements = new ArrayList<>();
        for (SerializableEventsDao serializableEventDao : serializableEventHolders){
            SerializableEventsDto serializableEventDto = serializableEventDao.getSerializableEventsDto();
            SaveAssertAttributeList saveAssertAttributeList = serializableEventDao.getAssertionSerializableEvents().getSaveAssertAttributeList();
            String event = serializableEventDto.getEvent();
            String eventOn = serializableEventDto.getEventOn();

            RecordedEventModel recordedEventModel = RecordedEventCreator.createRecordedEventModel(event,eventOn,driverManager,jsManager,executorManager);
            List<RecordedAssertModel> recordedAssertModel = createAssertionList(saveAssertAttributeList);
            WebRecordedElement webRecordedElement = new WebRecordedElement(serializableEventDto,recordedEventModel,recordedAssertModel);
            webRecordedElements.add(webRecordedElement);
        }
        return webRecordedElements;
    }

    private List<RecordedAssertModel> createAssertionList(SaveAssertAttributeList saveAssertAttributeList){
        if (saveAssertAttributeList != null){
            return RecordedAssertCreator.createRecordedAssertModel(saveAssertAttributeList.getXpaths(),saveAssertAttributeList.getAssertionTypeList(),jsManager,executorManager);
        }else{
            return new ArrayList<>();
        }

    }

}
