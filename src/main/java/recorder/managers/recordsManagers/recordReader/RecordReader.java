package recorder.managers.recordsManagers.recordReader;

import recorder.components.events.eventHolder.SerializableEventsDao;
import recorder.components.io.IORecord;

import java.io.*;
import java.util.ArrayList;


public class RecordReader extends IORecord {

    private ObjectInputStream inputStream;

    public RecordReader(){

    }

    public ArrayList<SerializableEventsDao> getTestRecord(String filename){
        try {
            filename = addFileExstension(filename);
            inputStream = new ObjectInputStream(new FileInputStream(open(filename)));
            return (ArrayList<SerializableEventsDao>) inputStream.readObject();
        }catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


}
