package recorder.managers.recordsManagers.recordWriter;


import recorder.components.events.eventHolder.SerializableEventsDao;
import recorder.components.io.IORecord;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class RecordWriter extends IORecord {
    private ObjectOutputStream outputStream;

    public RecordWriter(){

    }

    public void writeTestRecordToFile(String recordName,ArrayList<SerializableEventsDao> serializableEventHolders){
        try {
            recordName = addFileExstension(recordName);
            createFile(recordName);
            outputStream = new ObjectOutputStream(new FileOutputStream(open(recordName)));
            outputStream.writeObject(serializableEventHolders);
            outputStream.close();
            closeCurrentActiveRecord();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
