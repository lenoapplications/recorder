package recorder.managers.recordsManagers;

import async_communicator.thread_id_holder.ThreadIdHolder;
import recorder.managers.recordsManagers.recordReader.RecordReader;
import recorder.managers.recordsManagers.recordRunner.RecordRunner;
import recorder.managers.recordsManagers.recordRunner.RecordRunnerState;
import recorder.managers.recordsManagers.recordWriter.RecordWriter;
import recorder.managers.seleniumManager.SeleniumManager;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.components.records.recordHolder.RecordHolder;
import recorder.components.webRecordedElement.WebRecordedElement;
import thread_watcher.thread.caller.ThreadCaller;

import java.util.ArrayList;


public class RecordsManager {
    private final RecordWriter recordWriter;
    private final RecordReader recordReader;
    private final RecordRunner recordRunner;

    private final DriverManager driverManager;
    private RecordHolder currentActiveRecordHolder;

    public RecordsManager(DriverManager driverManager) {
        this.recordReader = new RecordReader();
        this.recordWriter = new RecordWriter();
        this.recordRunner = new RecordRunner();

        this.driverManager = driverManager;

    }

    public void saveTestRecord(String fileName){
        recordWriter.writeTestRecordToFile(fileName,currentActiveRecordHolder.getSerializableEventHolders());
    }
    public RecordRunnerState startTestRecord(SeleniumManager seleniumManager,String filename){
         ArrayList<WebRecordedElement> webRecordedElements = seleniumManager.initializationForRunningTestRecord(recordReader.getTestRecord(filename));
         return startRunnerThread(webRecordedElements);
    }
    public RecordHolder prepareNewTestRecord(){
        this.currentActiveRecordHolder = new RecordHolder();
        return this.currentActiveRecordHolder;
    }
    public void closeTestRecord(){
        this.currentActiveRecordHolder = null;
    }





    private RecordRunnerState startRunnerThread(ArrayList<WebRecordedElement> webRecordedElements){
        try {
            RecordRunnerState recordRunnerState = new RecordRunnerState();
            long threadId = ThreadCaller.getThreadCaller().callThread(RecordRunner.class,"runTestRecord",recordRunnerState,webRecordedElements,driverManager);
            recordRunnerState.setRunnerThread(new ThreadIdHolder(threadId));
            return recordRunnerState;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


}
