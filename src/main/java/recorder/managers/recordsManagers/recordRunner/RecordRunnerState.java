package recorder.managers.recordsManagers.recordRunner;

import async_communicator.thread_id_holder.ThreadIdHolder;
import recorder.components.exception.assertException.AssertException;
import recorder.components.exception.performException.PerformException;
import recorder.components.webRecordedElement.WebRecordedElement;

import java.util.ArrayList;
import java.util.List;

public class RecordRunnerState {

    public static class StateHolder{
        public int step;
        public WebRecordedElement webRecordedElement;
        public PerformException performException;
        public AssertException assertException;
        public String url;
    }

    private final List<StateHolder> stateHolderList;
    private boolean recordStarted = false;
    private boolean recordFinished = false;
    private boolean recordPassed = false;
    private ThreadIdHolder runnerThread;

    public RecordRunnerState() {
        this.stateHolderList = new ArrayList<>();
    }

    public void addNewStep(StateHolder stateHolder){
        stateHolderList.add(stateHolder);
    }

    public StateHolder getStep(int index){
        if (stateHolderList.size() > index){
            return stateHolderList.get(index);
        }
        return null;
    }



    public boolean isRecordStarted() {
        return recordStarted;
    }

    public void setRecordStarted(boolean recordStarted) {
        this.recordStarted = recordStarted;
    }

    public boolean isRecordFinished() {
        return recordFinished;
    }

    public void setRecordFinished(boolean recordFinished) {
        this.recordFinished = recordFinished;
    }

    public ThreadIdHolder getRunnerThread() {
        return runnerThread;
    }

    public void setRunnerThread(ThreadIdHolder runnerThread) {
        this.runnerThread = runnerThread;
    }

    public boolean isRecordPassed() {
        return recordPassed;
    }

    public void setRecordPassed(boolean recordPassed) {
        this.recordPassed = recordPassed;
    }

}
