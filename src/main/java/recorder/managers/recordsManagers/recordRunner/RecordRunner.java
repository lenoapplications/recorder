package recorder.managers.recordsManagers.recordRunner;

import recorder.components.exception.assertException.AssertException;
import recorder.components.exception.performException.PerformException;
import recorder.managers.seleniumManager.driverManager.DriverManager;
import recorder.components.exception.abstractException.ExceptionModel;
import recorder.components.webRecordedElement.WebRecordedElement;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;

import java.util.ArrayList;

public class RecordRunner extends UserMethod {
    private DriverManager driverManager;


    @ThreadMethod(paramNames = {"recordRunnerState","webRecordedElements","driverManager"})
    public void runTestRecord(Bundle bundle){
        this.driverManager = (DriverManager) bundle.getArguments("driverManager");
        RecordRunnerState recordRunnerState = (RecordRunnerState) bundle.getArguments("recordRunnerState");
        ArrayList<WebRecordedElement> webRecordedElements = (ArrayList<WebRecordedElement>) bundle.getArguments("webRecordedElements");

        recordRunnerState.setRecordStarted(true);
        for (int i = 0; i < webRecordedElements.size() ; ++i){
            System.out.println(webRecordedElements.get(i).getSerializableEventsDto().getEvent());
            if (!runStep(i,recordRunnerState,webRecordedElements.get(i))){
                recordRunnerState.setRecordFinished(true);
                recordRunnerState.setRecordPassed(false);
                return;
            }
        }
        recordRunnerState.setRecordFinished(true);
        recordRunnerState.setRecordPassed(true);
    }


    private boolean runStep(int step,RecordRunnerState recordRunnerState,WebRecordedElement webRecordedElement){
        try {
            webRecordedElement.perform();
            recordRunnerState.addNewStep(createRecordState(step,webRecordedElement));
        } catch (ExceptionModel exceptionModel){
            exceptionModel.setWebRecordedElement(webRecordedElement);
            if (!handleException(step,recordRunnerState,webRecordedElement,exceptionModel)){
                return false;
            }
        }
        return true;
    }
    private boolean handleException(int step,RecordRunnerState recordRunnerState,WebRecordedElement webRecordedElement,ExceptionModel exceptionModel){
        boolean status =  exceptionModel.provideSolution();
        recordRunnerState.addNewStep(createRecordState(step,exceptionModel,webRecordedElement));
        return status;
    }




    private RecordRunnerState.StateHolder createRecordState(int step, WebRecordedElement webRecordedElement){
        RecordRunnerState.StateHolder stateHolder = new RecordRunnerState.StateHolder();
        stateHolder.webRecordedElement = webRecordedElement;
        stateHolder.step = step;
        stateHolder.performException = null;
        stateHolder.assertException = null;
        stateHolder.url = driverManager.getWebDriver().getCurrentUrl();
        return stateHolder;
    }
    private RecordRunnerState.StateHolder createRecordState(int step,ExceptionModel exceptionModel ,WebRecordedElement webRecordedElement){
        RecordRunnerState.StateHolder stateHolder = new RecordRunnerState.StateHolder();
        stateHolder.webRecordedElement = webRecordedElement;
        stateHolder.step = step;
        stateHolder.url = driverManager.getWebDriver().getCurrentUrl();
        addExceptionToStateHolder(exceptionModel,stateHolder);
        return stateHolder;
    }

    private void addExceptionToStateHolder(ExceptionModel exceptionModel,RecordRunnerState.StateHolder stateHolder){
        if (exceptionModel instanceof PerformException){
            stateHolder.performException = (PerformException) exceptionModel;
            stateHolder.assertException = null;
        }else if (exceptionModel instanceof AssertException){
            stateHolder.assertException = (AssertException) exceptionModel;
            stateHolder.performException = null;
        }
    }

}
