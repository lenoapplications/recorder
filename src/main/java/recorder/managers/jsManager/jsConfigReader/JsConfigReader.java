package recorder.managers.jsManager.jsConfigReader;

import recorder.components.configuration.properties.JavascriptConfigurationProperties;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class JsConfigReader {
    private static String resourcePath = "./src/main/resources/static/javascript/configuration/JavascriptConfiguration.properties";

    private JsConfigReader(){}

    public static HashMap<String,String> javascriptConfiguration(){
        Properties properties = getProperties();
        return saveJsConfiguration(properties);
    }

    private static HashMap<String,String> saveJsConfiguration(Properties properties){
        HashMap<String,String> config = new HashMap<>();
        for (JavascriptConfigurationProperties property : JavascriptConfigurationProperties.values()){
            config.put(property.toString(),properties.getProperty(property.toString()));
        }
        return config;
    }

    private static Properties getProperties(){
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(resourcePath));
            Properties properties = new Properties();
            properties.load(fileInputStream);
            return properties;
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
