package recorder.managers.jsManager;

import org.openqa.selenium.JavascriptExecutor;
import recorder.managers.jsManager.jsReader.JsReader;
import recorder.managers.jsManager.userApiMethods.UserApi;
import recorder.managers.seleniumManager.executorManager.ExecutorManager;
import recorder.components.configuration.flags.JavascriptFlagsHolder;
import recorder.components.javascript.executorModel.ExecutorModel;


public class JsManager {
    private String stompLibCode;
    private String mainUploadCode;
    private String refreshCodeChecker;
    private String windowUnloadCodeChecker;
    private String waitForPresenceOfElement;
    private String assertionActivated;
    private String assertionDeactivated;
    private String jsTemplateFlagCode;

    private UserApi userApi;
    private final JsReader jsReader;
    private final JavascriptFlagsHolder javascriptFlagsHolder;

    public JsManager(){
        this.jsReader = new JsReader();
        javascriptFlagsHolder = new JavascriptFlagsHolder();
    }

    public void initializeJavascriptCodes(String windowName){
        mainUploadCode = jsReader.initMainJsCode(windowName);
        refreshCodeChecker = jsReader.initRefreshCodeChecker();
        windowUnloadCodeChecker = jsReader.initWindowUnloadChecker();
        waitForPresenceOfElement = jsReader.initWaitForPresenceOfElement();
        assertionActivated = jsReader.initAssertionActivated();
        assertionDeactivated = jsReader.initAssertionDeactivated();
        jsTemplateFlagCode = jsReader.initFlagsJsTemplateCode();
        stompLibCode = jsReader.initStomplibCode();
        userApi = new UserApi(jsReader.getUserApiCodes());

        javascriptFlagsHolder.init();
    }


    public <A> A uploadCode(ExecutorManager executorManager, String codeToUpload, Object...args){
        return (A) executorManager.executeJsCodeAndReturn(new ExecutorModel() {
            @Override
            public Object executeAndReturn(JavascriptExecutor javascriptExecutor) {
                return javascriptExecutor.executeScript(codeToUpload,args);
            }
        });
    }
    public <A> A uploadUserApiCode(ExecutorManager executorManager, String srcFile,String functionTag,String caller, Object...args){
        return (A) executorManager.executeJsCodeAndReturn(new ExecutorModel() {
            @Override
            public Object executeAndReturn(JavascriptExecutor javascriptExecutor) {
                String function = userApi.getFunction(srcFile,functionTag);
                function = function.concat(caller);
                return javascriptExecutor.executeScript(function,args);
            }
        });
    }

    public void uploadCompleteCode(ExecutorManager executorManager){
        executorManager.executeJsCode(new ExecutorModel() {
            @Override
            public void execute(JavascriptExecutor javascriptExecutor) {
                String flagCompleteCode = jsReader.putFlagsToCode(javascriptFlagsHolder.getFlags(),jsTemplateFlagCode);
                String completeCode = stompLibCode.concat(flagCompleteCode).concat(mainUploadCode);
                javascriptExecutor.executeScript(completeCode);
            }
        });
    }
    public void changeFlagInCode(ExecutorManager executorManager,String flag,Boolean value){
        executorManager.executeJsCode(new ExecutorModel() {
            @Override
            public void execute(JavascriptExecutor javascriptExecutor) {
                javascriptFlagsHolder.changeValue(flag,value);
                String code = String.format("window.mainObjectHolderForFlowMoveRecorder.flowMoveFlagsSignalization.%s = %s",flag,value.toString());
                javascriptExecutor.executeScript(code);
            }
        });
    }

    public void uploadAssertionActivated(ExecutorManager executorManager){
        executorManager.executeJsCode(new ExecutorModel() {
            @Override
            public void execute(JavascriptExecutor javascriptExecutor) {
                javascriptExecutor.executeScript(assertionActivated);
            }
        });
    }
    public void uploadAssertionDeactivated(ExecutorManager executorManager){
        executorManager.executeJsCode(new ExecutorModel() {
            @Override
            public void execute(JavascriptExecutor javascriptExecutor) {
                javascriptExecutor.executeScript(assertionDeactivated);
            }
        });
    }

    public Boolean uploadRefreshCheckerCode(ExecutorManager executorManager){
        return (Boolean) executorManager.executeJsCodeAndReturn(new ExecutorModel() {
            @Override
            public Object executeAndReturn(JavascriptExecutor javascriptExecutor) {
                return javascriptExecutor.executeScript(refreshCodeChecker);
            }
        });
    }
    public Boolean uploadWindowUnloadCheckerCode(ExecutorManager executorManager){
        return (Boolean) executorManager.executeJsCodeAndReturn(new ExecutorModel() {
            @Override
            public Object executeAndReturn(JavascriptExecutor javascriptExecutor) {
                return javascriptExecutor.executeScript(windowUnloadCodeChecker);
            }
        });
    }
    public Boolean uploadWaitForPresenceOfElement(ExecutorManager executorManager,String xpath){
        return (Boolean)executorManager.executeJsCodeAndReturn(new ExecutorModel() {
            @Override
            public Object executeAndReturn(JavascriptExecutor javascriptExecutor) {
                return javascriptExecutor.executeScript(waitForPresenceOfElement,xpath);
            }
        });
    }
}
