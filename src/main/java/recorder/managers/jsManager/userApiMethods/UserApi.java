package recorder.managers.jsManager.userApiMethods;

import java.util.HashMap;

public class UserApi {
    private final HashMap<String,String> userApi;

    public UserApi(HashMap<String, String> userApi) {
        this.userApi = userApi;
    }

    public String getFunction(String srcFile,String tagOfFunction){
        String src = userApi.get(srcFile);
        String beginTag = String.format("<%s>",tagOfFunction);
        String endTag = String.format("</%s>",tagOfFunction);

        int indexOfBegin = src.indexOf(beginTag) + beginTag.length();
        int indexOfEnd = src.indexOf(endTag);

        return src.substring(indexOfBegin,indexOfEnd);
    }
}
