package recorder.managers.jsManager.jsReader;

import recorder.managers.jsManager.jsConfigReader.JsConfigReader;
import recorder.components.configuration.flags.JavascriptFlags;
import recorder.components.configuration.properties.JavascriptConfigurationProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

public class JsReader {

    public String initMainJsCode(String windowName){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readFlowMoveObjectHolder(stringBuilder,jsPath);
            readSetup(stringBuilder,jsPath);
            readSocketFuncs(stringBuilder,jsPath);
            readListener(stringBuilder,jsPath);
            readUtilz(stringBuilder,jsPath);
            readMainUploadCaller(stringBuilder,jsPath);

            return putConfigurationToCode(windowName,stringBuilder);
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String initStomplibCode(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readStompLib(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public String initRefreshCodeChecker(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readRefreshCheckerCode(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String initWindowUnloadChecker(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readWindowUnloadChecker(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String initFlagsJsTemplateCode(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readJavascriptFlagsCode(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String initWaitForPresenceOfElement(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readWaitForPresenceOfElement(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String initAssertionActivated(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readAssertionActivated(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String initAssertionDeactivated(){
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            readAssertionDeactivated(stringBuilder,jsPath);
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashMap<String,String> getUserApiCodes(){
        try {
            HashMap<String,String> userApiCodes = new HashMap<>();
            String jsPath = System.getProperty("user.dir").concat("/src/main/resources/static/javascript/");
            userApiCodes.put("AssertionHelperFunctions.js",readUserApi("AssertionHelperFunctions.js",jsPath));

            return userApiCodes;
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public String putFlagsToCode(HashMap<String,Boolean> flags,String templateJsFlagCode){
        String allCode = new String(templateJsFlagCode);

        for(JavascriptFlags flag: JavascriptFlags.values()){
            String property = flag.getProperty();
            if (flags.containsKey(property)){
                allCode = allCode.replace(flag.getParameterSyntax(),flags.get(property).toString());
            }else{
                allCode = allCode.replace(flag.getParameterSyntax(),"false");
            }
        }
        return allCode;
    }



    private void readStompLib(StringBuilder stringBuilder, String dirPath) throws IOException {
        String jsPackage = "webSocket/";
        byte[] recorderSetup = readJsCode(new File(dirPath.concat(jsPackage.concat("Stomp.js"))));
        stringBuilder.append(new String(recorderSetup));
    }


    private void readFlowMoveObjectHolder(StringBuilder stringBuilder,String dirPath)throws IOException{
        String jsPackage = "src/flowMoveObjectHolder/";
        byte[] funcs = readJsCode(new File(dirPath.concat(jsPackage).concat("FlowMoveFuncsObjectHolder.js")));
        byte[] socket = readJsCode(new File(dirPath.concat(jsPackage).concat("FlowMoveSocketObjectHolder.js")));
        byte[] state = readJsCode(new File(dirPath.concat(jsPackage).concat("FlowMoveStateObjectHolder.js")));
        byte[] staticValues = readJsCode(new File(dirPath.concat(jsPackage).concat("FlowMoveStaticValuesObjectHolder.js")));
        byte[] mainObjectHolder = readJsCode(new File(dirPath.concat(jsPackage).concat("MainObjectHolder.js")));

        stringBuilder.append(new String(funcs));
        stringBuilder.append(new String(socket));
        stringBuilder.append(new String(state));
        stringBuilder.append(new String(staticValues));
        stringBuilder.append(new String(mainObjectHolder));

        addNewLine(stringBuilder);
    }

    private void readSetup(StringBuilder stringBuilder, String dirPath) throws IOException {
        String jsPackage = "src/recorderController/setup/";
        byte[] recorderSetup = readJsCode(new File(dirPath.concat(jsPackage.concat("RecorderSetup.js"))));
        stringBuilder.append(new String(recorderSetup));

        addNewLine(stringBuilder);
    }

    private void readSocketFuncs(StringBuilder stringBuilder, String dirPath) throws IOException {
        String jsPackage = "src/recorderController/functions/socket/";
        byte[] socketFunctions = readJsCode(new File(dirPath.concat(jsPackage.concat("SocketFunctions.js"))));

        stringBuilder.append(new String(socketFunctions));

        addNewLine(stringBuilder);
    }


    private void readListener(StringBuilder stringBuilder,String dirPath) throws IOException {
        String jsPackage = "src/recorderController/listeners/";
        byte[] documentListener = readJsCode(new File(dirPath.concat(jsPackage.concat("DocumentListeners.js"))));
        byte[] windowListener = readJsCode(new File(dirPath.concat(jsPackage.concat("WindowListener.js"))));

        stringBuilder.append(new String(documentListener));
        stringBuilder.append(new String(windowListener));

        addNewLine(stringBuilder);
    }

    private void readUtilz(StringBuilder stringBuilder,String dirPath) throws IOException {
        String jsPackage = "src/recorderController/functions/utilz/";
        byte[] eventCreator = readJsCode(new File(dirPath.concat(jsPackage).concat("EventCreatorObject.js")));
        byte[] webElementExtractor = readJsCode(new File(dirPath.concat(jsPackage).concat("WebElementExtractor.js")));
        stringBuilder.append(new String(eventCreator));
        stringBuilder.append(new String(webElementExtractor));

        addNewLine(stringBuilder);
    }

    private void readMainUploadCaller(StringBuilder stringBuilder, String dirPath) throws IOException {
        String mainPath = "src/caller/MainUploadCodeCaller.js";
        byte[] main = readJsCode(new File(dirPath.concat(mainPath)));
        stringBuilder.append(new String(main));

        addNewLine(stringBuilder);
    }

    private void readRefreshCheckerCode(StringBuilder stringBuilder,String dirPath) throws IOException {
        String jsPackage = "src/recorderController/functions/api/control/refreshPageControl/";
        byte[] refreshCodeChecker = readJsCode(new File(dirPath.concat(jsPackage.concat("RefreshCodeChecker.js"))));
        stringBuilder.append(new String(refreshCodeChecker));

        addNewLine(stringBuilder);
    }

    private void readWindowUnloadChecker(StringBuilder stringBuilder,String dirPath) throws IOException {
        String jsPackage = "src/recorderController/functions/api/control/refreshPageControl/";
        byte[] refreshCodeChecker = readJsCode(new File(dirPath.concat(jsPackage.concat("UnloadWindowCodeChecker.js"))));
        stringBuilder.append(new String(refreshCodeChecker));

        addNewLine(stringBuilder);

    }

    private void readWaitForPresenceOfElement(StringBuilder stringBuilder,String dirPath)throws IOException{
        String jsPackage = "src/recorderController/functions/api/control/performControl/";
        byte[] waitForPresence = readJsCode(new File(dirPath.concat(jsPackage.concat("WaitForPresenceOfElement.js"))));
        stringBuilder.append(new String(waitForPresence));

        addNewLine(stringBuilder);
    }
    private void readAssertionActivated(StringBuilder stringBuilder,String dirPath)throws IOException{
        String jsPackage = "src/recorderController/functions/api/control/assertionControl/";
        byte[] assertActive = readJsCode(new File(dirPath.concat(jsPackage.concat("AssertionActivated.js"))));
        stringBuilder.append(new String(assertActive));

        addNewLine(stringBuilder);

    }
    private void readAssertionDeactivated(StringBuilder stringBuilder,String dirPath)throws IOException{
        String jsPackage = "src/recorderController/functions/api/control/assertionControl/";
        byte[] assertDeactive = readJsCode(new File(dirPath.concat(jsPackage.concat("AssertionDeactivated.js"))));
        stringBuilder.append(new String(assertDeactive));

        addNewLine(stringBuilder);

    }

    private void readJavascriptFlagsCode(StringBuilder stringBuilder,String dirPath) throws IOException {
        String jsPackage = "src/flowMoveObjectHolder/";
        byte[] jsFlagsCode = readJsCode(new File(dirPath.concat(jsPackage.concat("FlowMoveFlagsObjectHolder.js"))));
        stringBuilder.append(new String(jsFlagsCode));

        addNewLine(stringBuilder);
    }
    private String readUserApi(String className ,String dirPath) throws IOException{
        String jsPackage = "src/recorderController/functions/api/userApi/";
        byte[] assertionHelperFunc = readJsCode(new File(dirPath.concat(jsPackage.concat(className))));
        return new String(assertionHelperFunc);
    }

    private String putConfigurationToCode(String windowName,StringBuilder stringBuilder){
        HashMap<String,String> config = JsConfigReader.javascriptConfiguration();
        String allCode = stringBuilder.toString();
        allCode = allCode.replace(JavascriptConfigurationProperties.ACTIVE_WINDOW_NAME.getParameterSyntax(),windowName);

        for (JavascriptConfigurationProperties property : JavascriptConfigurationProperties.values()){
            String parameterSyntax = property.getParameterSyntax();
            String value = config.get(property.getProperty());
            if (!property.getProperty().equals("<NOT_CONFIGURABLE>")){
                allCode = allCode.replace(parameterSyntax,value);
            }
        }
        return allCode;
    }



    private void addNewLine(StringBuilder stringBuilder){
        stringBuilder.append("\n");
    }

    private byte[] readJsCode(File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bytes = new byte[fileInputStream.available()];
        fileInputStream.read(bytes);
        return bytes;
    }
}
